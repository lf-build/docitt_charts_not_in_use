#!/usr/bin/env bash

    # Get root level directory information
    # () force it to be an array
    changeSets=(`git diff-tree --name-status HEAD`)

    # Loop through each array item
    for(( i=0; i<${#changeSets[@]}; i++))
    do
      # If current index is "M" or "A" then
      # Next index will contain name of the directory / file
      # NOTE: Only files on ROOT folder will be tracked.
      if [[ ${changeSets[$i]} == "M" || ${changeSets[$i]} == "A" ]]
      then
        # Extract path
        change=${changeSets[$i+1]}

        # If the path corresponds to a directory
        # (ignore if its a file -- because it will always be in root folder)
        if [[ -d "../$change" ]]
        then
          # See if the directory is a chart by makeing sure that
          # the directory contains Chart.yaml file
          if [[ -f "../$change/Chart.yaml" ]]
          then
            cd "../$change"
            # echo $change
            helm dep update .
            helm dep build .
            cd ../.build
            helm package "../$change"
          else
            echo "Not cool $change"
          fi
        fi
      fi
    done